class AddCoordinatesToMapPoints < ActiveRecord::Migration
  def change
  	add_column :map_points, :latitude, :float
  	add_column :map_points, :longitude, :float
  end
end
