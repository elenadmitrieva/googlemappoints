json.array!(@map_points) do |map_point|
  json.extract! map_point, :id
  json.url map_point_url(map_point, format: :json)
end
