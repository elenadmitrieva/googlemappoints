class MapPointsController < ApplicationController
  before_action :set_map_point, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @map_points = MapPoint.all
    respond_with(@map_points)
  end

  def show
    respond_with(@map_point)
  end

  def new
    @map_point = MapPoint.new
    respond_with(@map_point)
  end

  def edit
  end

  def create
    @map_point = MapPoint.new(map_point_params)
    @map_point.user = current_user
    @map_point.save
    redirect_to action: :index
  end

  def update
    @map_point.update(map_point_params)
    respond_with(@map_point)
  end

  def destroy
    @map_point.destroy
    respond_with(@map_point)
  end

  private
    def set_map_point
      @map_point = MapPoint.find(params[:id])
    end

    def map_point_params
      params.require(:map_point).permit(:latitude,:longitude)
    end
end
